#from os.path import join
from random import shuffle
from urllib.parse import urlparse

from apistar import Include, Route, annotate, render_template, Response
from apistar.frameworks.wsgi import WSGIApp as App
from apistar.handlers import docs_urls, static_urls
from apistar.renderers import HTMLRenderer
from apistar.typesystem import Array

from redis import StrictRedis

from settings import settings, env


url = urlparse(env['REDIS_URL'])
redis = StrictRedis(host=url.hostname, port=url.port, decode_responses=True)


#all_uiks = {'1', '2', '3'}

with open('uik_cams.csv', 'r') as f:
    all_uiks = set(x.split('\t')[0] for x in f.readlines())

print(f'{len(all_uiks)} UIKS loaded.')


class UiksReport(Array):
    items = str


def client_report(uiks: UiksReport):
    """
    Client sends report every 30s, with all uiks it is recording.
    """
    fresh_taken = set(redis.get('fresh_taken').split())
    taken = set(redis.get('taken').split())

    #print(uiks, taken, fresh_taken)
    for uik in uiks:
        if uik in all_uiks:
            taken.add(uik)
            fresh_taken.add(uik)

    redis.set('fresh_taken', ' '.join(fresh_taken))
    redis.set('taken', ' '.join(taken))

    return Response(str(uiks).encode('utf8'), content_type='text/plain')


@annotate(renderers=[HTMLRenderer()])
def show():
    taken = set(redis.get('taken').split())
    free = all_uiks - taken

    return render_template('list.html',
        taken=sorted(list(taken), key=int),
        free=sorted(list(free), key=int))


def take(num_wanted: int):
    """
    Client requests free uik numbers. We assume it will start recording those
    immediately.
    """
    fresh_taken = set(redis.get('fresh_taken').split())
    taken = set(redis.get('taken').split())

    free = all_uiks - taken

    to_give = list(free)[:num_wanted]

    if num_wanted > len(to_give):
        # All uiks covered and client wants more.
        # Give him up to num_wanted more, randomly chosen from already taken.
        rnd = list(taken)
        shuffle(rnd)
        num_overflow = num_wanted - len(to_give)
        to_give += rnd[:num_overflow]

    to_give = set(to_give)
    redis.set('fresh_taken', ' '.join(fresh_taken | to_give))
    redis.set('taken', ' '.join(taken | to_give))

    return Response(' '.join(to_give).encode('utf8'), content_type='text/plain')


routes = [
    Route('/', 'GET', show),
    Route('/take', 'POST', take),
    Route('/report', 'POST', client_report),
    Include('/docs', docs_urls),
    Include('/static', static_urls)
]

app = App(routes=routes, settings=settings)


if __name__ == '__main__':
    app.main()

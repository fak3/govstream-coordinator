# Govstream coordinator

Server, which coordinates recording uik numbers. Used with govstream-cli client app.

## Local deployment

```
docker-compose up
```

## Check that it works

Open http://127.0.0.1:8080/

run

```
sh test_report.sh
```

Refresh the page in the browser to make sure uiks 1 and 2 get taken. Note, after calling test_report.sh you have 30 seconds before uiks got released again.

from apistar.environment import Environment
from apistar.typesystem import boolean, string


class Env(Environment):
    properties = {
        'DEBUG': boolean(default=False),
        'REDIS_URL': string(default='redis://localhost:6379')
    }

env = Env()


settings = {
    'TEMPLATES': {
        'ROOT_DIR': '.',
        'PACKAGE_DIRS': ['apistar']  # Include the built-in apistar templates.
    }
}


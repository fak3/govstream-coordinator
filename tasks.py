import os

from celery import Celery

from settings import env
from app import redis


capp = Celery('tasks',
    broker=env['REDIS_URL'],
    backend=env['REDIS_URL'])

capp.conf.update(beat_schedule={
    'rotate': {
        'task': 'tasks.rotate_taken_uiks',
        'schedule': 30.0
    },
})


@capp.task(ignore_result=True)
def rotate_taken_uiks():
    """
    This task should be scheduled to launch periodically (every ~30s)
    Move `fresh_taken` to `taken`, then clean `fresh_taken`.
    So uiks that failed to be reported in last ~30s will get removed from `taken`.
    """
    fresh_taken = redis.get('fresh_taken')
    redis.set('taken', fresh_taken)
    redis.set('fresh_taken', '')


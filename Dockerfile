FROM python:3.6

ARG REQUIREMENTS=requirements.txt
RUN echo will use $REQUIREMENTS

WORKDIR "/app"

# These dependencies are baggage we found we need often enough to include
# them in our base image
RUN apt-get update && \
    apt-get install -y wget \
                       libpq-dev \
                       python-dev \
                       build-essential \
                       libxml2-dev \
                       libxslt1-dev \
                       libjpeg-dev \
                       graphviz \
                       graphviz-dev \
                       pkg-config \
                       postgresql-client \
                       git \
                       libgraphviz-dev \
                       libcgraph6 \
                       sudo \
                       nginx && \
    apt-get clean && \
    rm -rf /tmp/* /var/tmp/* /usr/share/man /tmp/* /var/tmp/* \
       /var/cache/apk/* /var/log/* /var/lib/apt/lists/* ~/.cache

# Create application user
RUN useradd --create-home --shell /bin/bash appuser

# Install python requirements
COPY ./requirements* /tmp/

RUN pip install -r /tmp/$REQUIREMENTS | tee -a /tmp/pip-requirements-install.log

COPY ./ /app

RUN chown -R appuser /app
USER appuser
